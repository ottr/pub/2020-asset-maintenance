#!/bin/bash

LUTRA_PATH='../lutra.jar'

LUTRA_EXEC='java -jar $LUTRA_PATH -I bottr -f'

OUTPUT='ontology'

mkdir -p $OUTPUT


## download Lutra if it does not exist
if [ ! -f "$LUTRA_PATH" ]; then
    wget https://gitlab.com/ottr/lutra/lutra/-/jobs/659476725/artifacts/raw/lutra.jar -O $LUTRA_PATH
fi

eval $LUTRA_EXEC -o $OUTPUT/iso14224 iso14224-mapping.ttl
eval $LUTRA_EXEC -o $OUTPUT/asset-model asset-model-mapping.ttl
eval $LUTRA_EXEC -o $OUTPUT/work-orders work-orders-mapping.ttl
